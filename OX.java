/**
 * OX
 */
import java.util.Scanner;

public class OX {

    static char board[][] = {{'-','-','-'},{'-','-','-'},{'-','-','-'}};
    static char turn = 'O';
    static int count = 0;
    static boolean gameEnd = false;

    public static void main(String[] args) {
        showWelcomeText();
        showBoard();
        gameProcess();

    }

    public static void showWelcomeText() {
        System.out.println("Welcome to OX Game");
    }

    public static void showBoard() {
        System.out.println();
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board.length; j++) {
                System.out.print(board[i][j]);
            }
            System.out.println();
        }
    }

    public static void gameProcess() {
        while(!gameEnd){
            showTurn();
            userInput();
            showBoard();
            checkWinLoseDraw();
        }
    }

    public static void showTurn(){
        System.out.println("Turn " + turn);
    }

    public static void userInput(){
        Scanner kb = new Scanner(System.in);
        while(true){
            try {
                System.out.println("Please input row, col:");
                int row = kb.nextInt();
                int col = kb.nextInt();
                if(board[row-1][col-1] == '-'){
                    board[row-1][col-1] = turn;
                    swapTurn();
                    break;
                }else{
                    System.out.println("Invalid input please try again");
                    showBoard();
                }
            } catch (Exception e) {
                System.out.println("Invalid input please try again");
                showBoard();
            }
        }
    }

    public static void swapTurn(){
        if(turn == 'O'){
            turn = 'X';
        }else{
            turn = 'O';
        }
    }

    public static void checkWinLoseDraw() {
        if(board[0][0] != '-' && board[0][0] == board[0][1] && board[0][1] == board[0][2]){
            System.out.println();
            System.out.println(">>>" + board[0][0] + " Win<<<");
            gameEnd = true;
        }else if(board[1][0] != '-' && board[1][0] == board[1][1] && board[1][1] == board[1][2]){
            System.out.println();
            System.out.println(">>>" + board[1][0] + " Win<<<");
            gameEnd = true;
        }else if(board[2][0] != '-' && board[2][0] == board[2][1] && board[2][1] == board[2][2]){
            System.out.println();
            System.out.println(">>>" + board[2][0] + " Win<<<");
            gameEnd = true;
        }else if(board[0][0] != '-' && board[0][0] == board[1][0] && board[1][0] == board[2][0]){
            System.out.println();
            System.out.println(">>>" + board[0][0] + " Win<<<");
            gameEnd = true;
        }else if(board[0][1] != '-' && board[0][1] == board[1][1] && board[1][1] == board[2][1]){
            System.out.println();
            System.out.println(">>>" + board[0][1] + " Win<<<");
            gameEnd = true;
        }else if(board[0][2] != '-' && board[0][2] == board[1][2] && board[1][2] == board[2][2]){
            System.out.println();
            System.out.println(">>>" + board[0][2] + " Win<<<");
            gameEnd = true;
        }else if(board[0][0] != '-' && board[0][0] == board[1][1] && board[1][1] == board[2][2]){
            System.out.println();
            System.out.println(">>>" + board[0][0] + " Win<<<");
            gameEnd = true;
        }else if(board[0][2] != '-' && board[0][2] == board[1][1] && board[1][1] == board[2][0]){
            System.out.println();
            System.out.println(">>>" + board[0][2] + " Win<<<");
            gameEnd = true;
        }else{
            count++;
            if(count == 9){
                System.out.println();
                System.out.println(">>>Draw<<<");
                gameEnd = true;
            }
        }
    }
}